#!/bin/bash

# 
# Sa 7. Mai 15:56:22 CEST 2022
# phelp help, man -k, info, (tldr? oder alternativen), suche nach weiteren docs? auf rechner und im internet?
# gibt es eine -h oder --help option?
# hilfe os-prober   # hilfe easyeffects -h / --help / --usage
# fehlende man/info pages?
# was ist mit flatpaks BSP:SIGNAL ? appimages? kdehelp?
# type oder which gedöns falls alles scheitert? sonst was ist noch an docs offline vorhanden?

# eigene optionen für ort der manpages infopages... etc.   außerdem, ort des programms 
# option automatisch ausführen wenn sonst nix gefunden wird

# die optionen von file, type, whatis, apropos, whereis   <- genauer angucken
# wtf ist getent?  die lösung für nach dateien suchen?
# openbsd manpages?

# stackoverflow?  eigene clents für tldr oder sowas?
# pinfo für cheatsheat?


# apropos $hilfe_argument | sed 's/ .*//' | xargs whereis | grep $hilfe_argument --color=auto

if [ $LANG = "de_DE.UTF-8" ]; then
read -r -d '' hilfe_nachricht <<'EOF'

'hilfe.sh' listet alle verfügbaren Dokumentationen für SCHLÜSSELBEGRIFF auf
 wenn die Ausgabe zu lang ist pipe nach 'less -R' um die Farben zu erhalten
	QUELLEN = whereis, find (/usr/share/doc), help, man, info, cht.sh

hilfe.sh [-h|--help|hilfe]
hilfe.sh [OPTION]... SCHLÜSSELBEGRIFF

 -h, --help, hilfe
	zeige diese Hilfeseite

 -w, (Voreinstellung)
	ahmt "whatis" nach, aber für alle verfügbaren Dokumentationen
	 EXECUTED COMMANDS:
		whereis SCHLÜSSELBEGRIFF
		find /usr/share/doc -name SCHLÜSSELBEGRIFF
		help -d SCHLÜSSELBEGRIFF
		man -f SCHLÜSSELBEGRIFF
		info -w SCHLÜSSELBEGRIFF
		cht.sh :list | grep -x SCHLÜSSELBEGRIFF

-a
	ahmt "apropos" nach, aber für alle verfügbaren Dokumentationen
	 EXECUTED COMMANDS:
		whereis SCHLÜSSELBEGRIFF
		find /usr/share/doc -name *SCHLÜSSELBEGRIFF*
		help -d SCHLÜSSELBEGRIFF
		man -k SCHLÜSSELBEGRIFF
		info -k SCHLÜSSELBEGRIFF
		cht.sh :list | grep SCHLÜSSELBEGRIFF
EOF
else
read -r -d '' hilfe_nachricht <<'EOF'

'hilfe.sh' lists all available documentation for KEYWORD
 if the output is too long, pipe it to 'less -R' to preserve colours
	SOURCES = whereis, find (/usr/share/doc), help, man, info, cht.sh

hilfe.sh [-h|--help|hilfe]
hilfe.sh [OPTION]... KEYWORD

 -h, --help, hilfe
	show this help page

 -w, (default)
	mimics "whatis", but for all available documentation
	 EXECUTED COMMANDS:
		whereis KEYWORD
		find /usr/share/doc -name KEYWORD
		help -d KEYWORD
		man -f KEYWORD
		info -w KEYWORD
		cht.sh :list | grep -x KEYWORD

-a
	mimics "apropos", but for all available documentation
	 EXECUTED COMMANDS:
		whereis KEYWORD
		find /usr/share/doc -name *KEYWORD*
		help -d KEYWORD
		man -k KEYWORD
		info -k KEYWORD
		cht.sh :list | grep KEYWORD
EOF
fi

info_opt='-w'
man_opt='-f'
help_opt='-d'
cht_grep_opt='-x'

# nicht nur executable files??
# online docs
# übersetzung verbessern?   line break einfügen?

function hilfe_befehl() {
if [ $LANG = "de_DE.UTF-8" ]; then
	echo -e "\033[38;5;11m!!! NICHT VERGESSEN: viele ausführbare Dateien bieten kurze Dokumentationen \n\tdurch Optionen wie '-h', '--help' oder '--usage'\033[00m"
else
	echo -e "\033[38;5;11m!!! DO NOT FORGET: many executable files provide short documentation \n\tthrough options like '-h', '--help' or '--usage'\033[00m"
fi
	echo -e "---\033[38;5;14m \$ whereis \033[04;38;5;14m$hilfe_argument\033[00m ---"
	whereis $hilfe_argument | sed "s/$hilfe_argument/\x1b[01;31m&\x1b[00m/g"
	echo -e "---\033[38;5;14m \$ find /usr/share/doc -name \033[04;38;5;14m$find_argument\033[00m ---"
	find /usr/share/doc -name $find_argument | sed "s/$hilfe_argument/\x1b[01;31m&\x1b[00m/g"
	echo -e "---\033[38;5;14m \$ help $help_opt \033[04;38;5;14m$hilfe_argument\033[00m ---"
	help $help_opt $hilfe_argument | sed "s/$hilfe_argument/\x1b[01;31m&\x1b[00m/g"
	help_error=$?
	echo -e "---\033[38;5;14m \$ man $man_opt \033[04;38;5;14m$hilfe_argument\033[00m ---"
	man $man_opt $hilfe_argument | sed "s/$hilfe_argument/\x1b[01;31m&\x1b[00m/g"
	man_error=$?
	echo -e "---\033[38;5;14m \$ info $info_opt \033[04;38;5;14m$hilfe_argument\033[00m ---"
	info $info_opt $hilfe_argument | sed "s/$hilfe_argument/\x1b[01;31m&\x1b[00m/g"
	info_error=$?
	echo -e "---\033[38;5;14m \$ cht.sh :list | grep $cht_grep_opt \033[04;38;5;14m$hilfe_argument\033[00m ---"
	cht.sh :list | grep $cht_grep_opt $hilfe_argument | sed "s/$hilfe_argument/\x1b[01;31m&\x1b[00m/g"
	cht_error=$?
	if [ "$info_error" != 0 ] && [ "$man_error" != 0 ] && [ "$help_error" != 0 ] && [ "$cht_error" != 0 ]; then
		echo "weitere optionen, intenetsuchen alias, doc empfehlungen, das find ding?<- eher option"
	fi
}

if [ $# -eq 0 ] || [ $1 == "hilfe" ] || [ $1 == "--help" ]; then
	echo "$hilfe_nachricht"
else
	while getopts hw:a: opt; do
		hilfe_argument=$OPTARG
		case $opt in
			h) echo "$hilfe_nachricht"
			;;
			w) find_argument=$hilfe_argument
			   hilfe_befehl
			;;
			a) info_opt='-k'
			   man_opt='-k'
			   help_opt='-d'
			   cht_grep_opt=''
			   find_argument=*${hilfe_argument}*
			   hilfe_befehl
			;;
		esac
	done
	if [ $OPTIND -eq 1 ]; then
		hilfe_argument=$1
		find_argument=$hilfe_argument
		hilfe_befehl
	fi
fi


